import '../assets/scss/App.css';
import {useReducer} from 'react';
import {todoReducer} from '../reducer/todoReducer';

const initalState = [
  {
    id: new Date().getTime(),
    desc: 'Aprender React',
    donde: false
  }
]

export const App = () => {


  const [todos] = useReducer(todoReducer, initalState);
  console.log(todos);
  return (
    <div className="App">
      <h1>To Do ({todos.length})</h1>
      <hr />
      <ul>
        {
          todos.map(todo => (
            <li key={todo.id}>
              {
                todo.desc
              }</li>
          ))
        }</ul>
    </div>
  );
}

export default App;
